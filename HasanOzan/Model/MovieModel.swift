//
//  MovieModel.swift
//  HasanOzan
//
//  Created by Ozan Al on 7.05.2019.
//  Copyright © 2019 Ozan Al. All rights reserved.
//

import Foundation

struct MovieModel {
    
    var voteCount: Int?
    var id: Int?
    var video: Bool?
    var voteAverage: Double?
    var title: String?
    var popularity: Double?
    var posterPath: String?
    var originalTitle: String?
    var detailImage: String?
    var overviewDetail: String?
    var date: String?
    var isMovieFavorited: Bool?
    
    init(cellModel: NSDictionary) {
        self.voteCount = cellModel.value(forKey: "vote_count") as? Int
        self.id = cellModel.value(forKey: "id") as? Int
        self.video = cellModel.value(forKey: "video") as? Bool
        self.voteAverage = cellModel.value(forKey: "vote_average") as? Double
        self.title = cellModel.value(forKey: "title") as? String
        self.popularity = cellModel.value(forKey: "popularity") as? Double
        self.posterPath = cellModel.value(forKey: "poster_path") as? String
        self.originalTitle = cellModel.value(forKey: "original_title") as? String
        self.detailImage = cellModel.value(forKey: "backdrop_path") as? String
        self.overviewDetail = cellModel.value(forKey: "overview") as? String
        self.date = cellModel.value(forKey: "date") as? String
    }
    
}
