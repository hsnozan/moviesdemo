//
//  HomeCollectionViewCell.swift
//  HasanOzan
//
//  Created by Ozan Al on 8.05.2019.
//  Copyright © 2019 Ozan Al. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var movieLabel: UILabel!
    @IBOutlet weak var movieFavoritedImage: UIImageView!
}
