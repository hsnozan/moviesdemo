//
//  ViewController.swift
//  HasanOzan
//
//  Created by Ozan Al on 7.05.2019.
//  Copyright © 2019 Ozan Al. All rights reserved.
//

import UIKit
import Kingfisher

class HomeViewController: UIViewController {

    @IBOutlet weak var collectionTableView: UICollectionView!
    @IBOutlet weak var homeTableView: UITableView!
    var results: NSArray?
    var movieModelArray = [MovieModel]()
    var movieModel: MovieModel!
    var pageCount = 2
    var navigationImageButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationItem()
        getJSONArrayFromURL()
    }
    
    func setNavigationItem() {
        navigationImageButton = UIButton.init(type: .custom)
        navigationImageButton.setImage(UIImage.init(named: "collectionView"), for: UIControl.State.normal)
        navigationImageButton.addTarget(self, action:#selector(HomeViewController.onListViewToggleClick(_:)), for:.touchUpInside)
        navigationImageButton.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem.init(customView: navigationImageButton)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    func getJSONArrayFromURL() {
        URLSession.shared.dataTask(with: URL(string: "https://api.themoviedb.org/3/movie/popular?language=en-%20%20US&api_key=fd2b04342048fa2d5f728561866ad52a&page=" + String(pageCount))!) { (data, response, error) -> Void in
            if error == nil && data != nil {
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [String: Any]
                    self.results = json["results"] as? NSArray
                    for model in self.results! {
                        let cellModel = model as! NSDictionary
                        let movie = MovieModel(cellModel: cellModel)
                        self.movieModelArray.append(movie)
                    }
                    DispatchQueue.main.async {
                        self.homeTableView.reloadData()
                        self.collectionTableView.reloadData()
                    }
                } catch {
                    print("JSON could not parse")
                }
            }
        }.resume()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toHomeDetailVC" {
            if let indexPath = homeTableView.indexPathForSelectedRow {
                if let homeDetailVC = segue.destination as? HomeDetailViewController {
                    let dictionaryModel = movieModelArray[indexPath.row]
                    homeDetailVC.selectionDelegate = self
                    homeDetailVC.movieModel = dictionaryModel
                    homeDetailVC.indexPath = indexPath
                }
            }
        } else if segue.identifier == "toHomeCollectionDetailVC" {
            if let homeDetailVC = segue.destination as? HomeDetailViewController {
                let cell = sender as! HomeCollectionViewCell
                let indexPath = collectionTableView.indexPath(for: cell)
                let dictionaryModel  = movieModelArray[indexPath?.row ?? 0]
                homeDetailVC.selectionDelegate = self
                homeDetailVC.movieModel = dictionaryModel
                homeDetailVC.indexPath = indexPath
            }
        }
    }
    
    @objc func onListViewToggleClick(_ sender: Any) {
        if homeTableView.isHidden {
            homeTableView.isHidden = false
            collectionTableView.isHidden = true
            navigationImageButton.setImage(UIImage.init(named: "collectionView"), for: UIControl.State.normal)
        } else {
            homeTableView.isHidden = true
            collectionTableView.isHidden = false
            navigationImageButton.setImage(UIImage.init(named: "listView"), for: UIControl.State.normal)
        }
    }
    
    @objc func onLoadMoreButtonClick(_ sender: Any) {
        pageCount += 1
        getJSONArrayFromURL()
    }
}

extension HomeViewController: MovieFavoriteSelectionDelegate {
    
    func didMovieFavorited(movieModel: MovieModel, indexPath: IndexPath) {
        movieModelArray[indexPath.row] = movieModel
        homeTableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.none)
        collectionTableView.reloadItems(at: [indexPath])
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movieModelArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell") as! HomeTableViewCell
        let cellModel = movieModelArray[indexPath.row]
        let fullUrl = cellModel.posterPath
        let url = URL(string: "https://image.tmdb.org/t/p/w500" + fullUrl!)
        cell.movieLabel.text = cellModel.title
        cell.movieImage.kf.setImage(with: url)
        
        if cellModel.isMovieFavorited ?? false {
            cell.movieFavoritedImage.isHidden = false
        } else {
            cell.movieFavoritedImage.isHidden = true
        }
        
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
            let tableViewBottomButton = UIButton.init(type: .custom)
            tableViewBottomButton.setTitle("Load More", for: UIControl.State.normal)
            tableViewBottomButton.setTitleColor(#colorLiteral(red: 0.2549019608, green: 0.6117647059, blue: 1, alpha: 1), for: UIControl.State.normal)
            tableViewBottomButton.addTarget(self, action:#selector(HomeViewController.onLoadMoreButtonClick(_:)), for:.touchUpInside)
            tableViewBottomButton.frame = CGRect.init(x: 0, y: 0, width: 50, height: 30)
            tableViewBottomButton.topAnchor.constraint(equalTo: homeTableView.bottomAnchor, constant: 30)
            self.homeTableView.tableFooterView = tableViewBottomButton
            self.homeTableView.tableFooterView?.isHidden = false
        }
        
        if indexPath.row == movieModelArray.count - 1 {
            
        }
        
        return cell
    }
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movieModelArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
        let cellModel = movieModelArray[indexPath.row]
        let fullUrl = cellModel.posterPath
        let url = URL(string: "https://image.tmdb.org/t/p/w500" + fullUrl!)
        cell.movieLabel.text = cellModel.title
        cell.movieImage.kf.setImage(with: url)
        if cellModel.isMovieFavorited ?? false {
            cell.movieFavoritedImage.isHidden = false
        } else {
            cell.movieFavoritedImage.isHidden = true
        }
        
//        let lastSectionIndex = collectionView.numberOfSections - 1
//        let lastRowIndex = collectionView.numberOfItems(inSection: lastSectionIndex) - 1
//        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
//            let tableViewBottomButton = UIButton.init(type: .custom)
//            tableViewBottomButton.setTitle("Load More", for: UIControl.State.normal)
//            tableViewBottomButton.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: UIControl.State.normal)
//            tableViewBottomButton.backgroundColor = #colorLiteral(red: 1, green: 0.3098039216, blue: 0.2666666667, alpha: 1)
//            tableViewBottomButton.addTarget(self, action:#selector(HomeViewController.onLoadMoreButtonClick(_:)), for:.touchUpInside)
//            tableViewBottomButton.frame = CGRect.init(x: 0, y: collectionTableView.frame.height, width: 100, height: 50)
//            view.addSubview(tableViewBottomButton)
//        }
        return cell
    }
  
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width / 2.0
        let height = width
        return CGSize(width: width, height: height)
    }
    
    
}

