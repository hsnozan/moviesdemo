//
//  HomeDetailViewController.swift
//  HasanOzan
//
//  Created by Ozan Al on 8.05.2019.
//  Copyright © 2019 Ozan Al. All rights reserved.
//

import UIKit

protocol MovieFavoriteSelectionDelegate {
    
    func didMovieFavorited(movieModel: MovieModel, indexPath: IndexPath)
    
}

class HomeDetailViewController: UIViewController {

    @IBOutlet weak var movieVote: UILabel!
    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var movieName: UILabel!
    @IBOutlet weak var movieOverview: UILabel!
    @IBOutlet weak var starImage: UIImageView!
    var movieModel: MovieModel?
    var isFavorited: Bool?
    var indexPath: IndexPath!
    var selectionDelegate: MovieFavoriteSelectionDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setLayout()
    }
    
    func setLayout() {
        let fullUrl = movieModel?.posterPath  ?? ""
        let url = URL(string: "https://image.tmdb.org/t/p/w500" + fullUrl)
        movieImage.kf.setImage(with: url)
        movieName.text = movieModel?.title
        movieOverview.text = movieModel?.overviewDetail
        let voteAverage = movieModel?.voteAverage ?? 5.0
        movieVote.text = "Rating: " + String(voteAverage)
        
        isFavorited = movieModel?.isMovieFavorited ?? false
        if isFavorited! {
            starImage.image = UIImage(named: "filledStar")
        }
    }

    @IBAction func onStarClick(_ sender: Any) {
        if isFavorited! {
            starImage.image = UIImage(named: "emptyStar")
            movieModel?.isMovieFavorited = false
            isFavorited = false
        } else {
            starImage.image = UIImage(named: "filledStar")
            movieModel?.isMovieFavorited = true
            isFavorited = true
        }
        
        selectionDelegate.didMovieFavorited(movieModel: movieModel!, indexPath: indexPath)
    }
}
